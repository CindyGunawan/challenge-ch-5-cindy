package com.example.myapplication

import android.content.res.ColorStateList
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var textView = findViewById<TextView>(R.id.resultGame)
        textView.textAlignment = View.TEXT_ALIGNMENT_CENTER


        buttonReset.setOnClickListener {
            clear_score()
        }


        gunting.setOnClickListener {

            val computer_move = (1..4).random() // 4 is not included.


            if (computer_move == 1) {
                batucom.setImageDrawable(getResources().getDrawable(R.drawable.batu));
                batucom.setBackgroundColor(getResources().getColor(R.color.lightgrey));

                resultGame.text = "COM WIN"
                resultGame.setTextColor(getResources().getColor(R.color.white))
                resultGame.setBackgroundResource(R.drawable.bg_shape_player);

            } else if (computer_move == 2) {

                kertascom.setImageDrawable(getResources().getDrawable(R.drawable.kertas));
                kertascom.setBackgroundColor(getResources().getColor(R.color.lightgrey));

                resultGame.text = "PLAYER WIN"
                resultGame.setTextColor(getResources().getColor(R.color.white))
                resultGame.setBackgroundResource(R.drawable.bg_shape_player);


            } else {

                guntingcom.setImageDrawable(getResources().getDrawable(R.drawable.gunting));
                guntingcom.setBackgroundColor(getResources().getColor(R.color.lightgrey));

                resultGame.text = "DRAW"
                resultGame.setTextColor(getResources().getColor(R.color.white))
                resultGame.setBackgroundResource(R.drawable.bg_shape_draw);
            }

        }

        kertas.setOnClickListener {


            val computer_move = (1..4).random()


            if (computer_move == 1) {
                batucom.setImageDrawable(getResources().getDrawable(R.drawable.batu));
                batucom.setBackgroundColor(getResources().getColor(R.color.lightgrey));

                resultGame.text = "PLAYER WIN"
                resultGame.setTextColor(getResources().getColor(R.color.white))
                resultGame.setBackgroundResource(R.drawable.bg_shape_player);


            } else if (computer_move == 2) {


                kertascom.setImageDrawable(getResources().getDrawable(R.drawable.kertas));
                kertascom.setBackgroundColor(getResources().getColor(R.color.lightgrey));


                resultGame.text = "DRAW"
                resultGame.setTextColor(getResources().getColor(R.color.white))
                resultGame.setBackgroundResource(R.drawable.bg_shape_player);


            } else {

                guntingcom.setImageDrawable(getResources().getDrawable(R.drawable.gunting));
                guntingcom.setBackgroundColor(getResources().getColor(R.color.lightgrey));

                resultGame.text = "COM WIN"
                resultGame.setTextColor(getResources().getColor(R.color.white))
                resultGame.setBackgroundResource(R.drawable.bg_shape_draw);
            }

        }

        // when player click on rock icon.
        batu.setOnClickListener {

            val computer_move = (1..4).random()


            if (computer_move == 1) {
                // set the image of computer move to rock
                batucom.setImageDrawable(getResources().getDrawable(R.drawable.batu));
                batucom.setBackgroundColor(getResources().getColor(R.color.lightgrey));

                // rock beats scissors.
                resultGame.text = "DRAW"
                resultGame.setTextColor(getResources().getColor(R.color.white))
                resultGame.setBackgroundResource(R.drawable.bg_shape_player);

            } else if (computer_move == 2) {

                // set the image of computer move to paper
                kertascom.setImageDrawable(getResources().getDrawable(R.drawable.kertas));
                kertascom.setBackgroundColor(getResources().getColor(R.color.lightgrey));

                // scissors beats paper
                resultGame.text = "COM WIN"
                resultGame.setTextColor(getResources().getColor(R.color.white))
                resultGame.setBackgroundResource(R.drawable.bg_shape_player);


            } else {

                // set the image of computer move to scissors
                guntingcom.setImageDrawable(getResources().getDrawable(R.drawable.gunting));
                guntingcom.setBackgroundColor(getResources().getColor(R.color.lightgrey));

                // both user move and computer move are "scissors"
                resultGame.text = "PLAYER WIN"
                resultGame.setTextColor(getResources().getColor(R.color.white))
                resultGame.setBackgroundResource(R.drawable.bg_shape_draw);
            }

        }
    }


    private fun clear_score() {


        resultGame.text = "VS"
        resultGame.setTextColor(getResources().getColor(R.color.red))
        resultGame.setBackgroundResource(R.drawable.bg_shape_vs);

        batucom.setBackgroundColor(getResources().getColor(R.color.white));
        kertascom.setBackgroundColor(getResources().getColor(R.color.white));
        guntingcom.setBackgroundColor(getResources().getColor(R.color.white));


    }
}

